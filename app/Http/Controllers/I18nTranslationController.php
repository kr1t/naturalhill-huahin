<?php

namespace App\Http\Controllers;

use App\I18nTranslation;
use Illuminate\Http\Request;

class I18nTranslationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\I18nTranslation  $i18nTranslation
     * @return \Illuminate\Http\Response
     */
    public function show(I18nTranslation $i18nTranslation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\I18nTranslation  $i18nTranslation
     * @return \Illuminate\Http\Response
     */
    public function edit(I18nTranslation $i18nTranslation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\I18nTranslation  $i18nTranslation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, I18nTranslation $i18nTranslation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\I18nTranslation  $i18nTranslation
     * @return \Illuminate\Http\Response
     */
    public function destroy(I18nTranslation $i18nTranslation)
    {
        //
    }
}
