<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Work extends Model
{
    use SoftDeletes;
    protected $appends = ['created_at_text'];
    protected $fillable = ['name', 'label','description','work_category_id','client_id'];
    public function getCreatedAtTextAttribute()
    {
        return $this->created_at ? \Carbon\Carbon::parse($this->created_at)->format('d/m/Y H:i:s') : '-';
    }

    public function images()
    {
        return $this->hasMany('App\WorkImage');
    }

      public function category()
    {
        return $this->belongsTo('App\Models\WorkCategory','work_category_id','id');
    }
        public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }
}
