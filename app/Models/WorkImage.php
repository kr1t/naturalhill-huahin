<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkImage extends Model
{
    use SoftDeletes;
    protected $fillable = ['image_url','work_id'];
}
