<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('label');
            $table->text('description');
              $table->bigInteger('work_category_id')->unsigned()->default(1);
            $table->foreign('work_category_id')
                ->references('id')
                ->on('work_categories')
                ->onDelete('cascade');
            $table->bigInteger('client_id')->unsigned()->default(1);
            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('cascade');
                            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
