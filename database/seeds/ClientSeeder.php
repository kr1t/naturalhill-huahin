<?php

use Illuminate\Database\Seeder;
use App\Models\Client;
class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::insert([
            [
                'name'=> 'test1',
                'image_url' => 'https://dummyimage.com/960x520/28a746/ffffff'
            ],
              [
                'name'=> 'test2',
                'image_url' => 'https://dummyimage.com/960x520/28a746/ffffff'
            ],
              [
                'name'=> 'test3',
                'image_url' => 'https://dummyimage.com/960x520/28a746/ffffff'
            ]
        ]);
    }
}
