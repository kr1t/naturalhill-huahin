<?php

use App\Model\WorkCategory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([RoleSeeder::class,  UserSeeder::class, WorkCategorySeeder::class,ClientSeeder::class]);
    }
}
