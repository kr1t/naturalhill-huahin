<?php

use Illuminate\Database\Seeder;
use App\Models\WorkCategory;
class WorkCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WorkCategory::insert([
            [
                "name"=>'Web Production',
                "slug"=> 'web_productions'
            ],
             [
                "name"=>'Mobile Application',
                "slug"=> 'mobiles'
            ],
             [
                "name"=>'Banner & Animation',
                "slug"=> 'banner_animation'
            ],
             [
                "name"=>'Interactive Events',
                "slug"=> 'interactive events'
            ]
        ]);
    }
}
