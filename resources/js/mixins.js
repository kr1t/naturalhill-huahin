const folderDirectory = window.config.fDir;
const assetDirectory = window.config.aDir;
import axios from "axios";

import { mapGetters } from "vuex";
export default {
  computed: {
    ...mapGetters({
      user: "auth/user"
    })
  },
  methods: {
    aUrl: p => `${folderDirectory}${p}`,
    handleImageAdded: function(file, Editor, cursorLocation, resetUploader) {
      var formData = new FormData();
      formData.append("uploadFileObj", file);

      axios({
        url: this.$api("image/upload"),
        method: "POST",
        data: formData
      })
        .then(result => {
          let url = result.data.url; // Get url from response
          Editor.insertEmbed(cursorLocation, "image", url);
          resetUploader();
        })
        .catch(err => {
          console.log(err);
        });
    }
  }
};
