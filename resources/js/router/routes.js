function page(path) {
  return () =>
    import(/* webpackChunkName: '' */ `~/pages/${path}`).then(
      m => m.default || m
    );
}

function cruGen(path, name) {
  return [
    {
      path: `${path}`,
      name: path,
      component: page(`admin/${name}/${name}.vue`)
    },
    {
      path: `${path}/create`,
      name: `${path}.create`,
      component: page(`admin/${name}/form.vue`)
    },
    {
      path: `${path}/:id/edit`,
      name: `${path}.edit`,
      component: page(`admin/${name}/form.vue`)
    }
  ];
}

export default [
    {
      path: "/login",
      name: "login",
      component: page("auth/login.vue")
    },
    {
      path: "/register",
      name: "register",
      component: page("auth/register.vue")
    },
    {
      path: "/",
      name: "home",
      component: page("home.vue")
    },
    {
      path: "/design",
      name: "design",
      component: page("Design.vue")
    },
    {
      path: "/design/:key",
      name: "designs",
      component: page("Design.vue")
    },
    {
      path: "/location",
      name: "location",
      component: page("Location.vue")
    },
    {
      path: "/plotmap",
      name: "plotmap",
      component: page("Plotmap.vue")
    },
    {
      path: "/progressive",
      name: "progressive",
      component: page("Progressive.vue")
    },
    {
      path: "/promotion",
      name: "promotion",
      component: page("Promotion.vue")
    },
    {
      path: "/payment",
      name: "payment",
      component: page("Payment.vue")
    }
    ,
    {
      path: "/gallery",
      name: "gallery",
      component: page("Gallery.vue")
    },
    {
      path: "/forrent",
      name: "forrent",
      component: page("ForRent.vue")
    },
    {
      path: "/service",
      name: "service",
      component: page("Service.vue")
    },
    {
      path: "/contact",
      name: "contact",
      component: page("Contact.vue")
    },
    {
      path: "/info",
      name: "info",
      component: page("Info.vue")
    },
    {
      path: "/about",
      name: "about",
      component: page("About.vue")
    },

  {
    path: "/settings",
    component: page("settings/index.vue"),
    children: [
      {
        path: "",
        redirect: {
          name: "settings.profile"
        }
      },
      {
        path: "profile",
        name: "settings.profile",
        component: page("settings/profile.vue")
      },
      {
        path: "password",
        name: "settings.password",
        component: page("settings/password.vue")
      }
    ]
  },
  {
    path: "/admin",
    component: page("admin/index.vue"),
    children: [
      {
        path: "",
        name: "adminHome",
        component: page("admin/adminDashboard.vue")
      },
      ...cruGen("work_categories", "workCategory"),
      ...cruGen("clients", "client"),
      ...cruGen("works", "work")
    ]
  },



  {
    path: "*",
    component: page("errors/404.vue")
  }
];
